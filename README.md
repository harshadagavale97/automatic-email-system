    Named Entity Recognization using spaCy

    1: The first python program spacy.py is simple Named Entity Recognization Program using Spacy.
            In which the input is text and output is Named Entity.
        It can classify the text into pre-defined categories like person,organization,locations,etc.
        So spacy.py is simple program using spacy

    2: The second python program custom_entity1.py 
            The above first program may recognize wrong categories.
        To avoid this problem, we have to train the spacy model by giving it training data.
        In this program, we provided a small training data to train the spacy model to handle the problem.

    3: The third program is custom_entity2.py
            There are limited categories in spacy model.
        But we can add new categories to blank model by training it.
        In this program, I added new category named 'FOOD' by providing training data.
        